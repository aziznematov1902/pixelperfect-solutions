import React from 'react';
import './App.css';

function App() {
  return (
    <div className="app">
      <Header />
      <HomePage />
      <Footer />
    </div>
  );
}

const Header = () => {
  return (
    <header className="header">
      <div className="logo">Pixel Perfect Solutions</div>
      <nav>
        <ul>
          <li><a href="#home">Home</a></li>
          <li><a href="#about">About</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portfolio">Portfolio</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
  );
};

const HomePage = () => {
  return (
    <div>
      <HeroSection />
      <AboutSection />
      <ServicesSection />
      <PortfolioSection />
      <ContactSection />
    </div>
  );
};

const HeroSection = () => {
  return (
    <section id="home" className="hero">
      <h1>Welcome to Pixel Perfect Solutions</h1>
      <p>Your partner in digital transformation</p>
      <button>Get Started</button>
    </section>
  );
};

const AboutSection = () => {
  return (
      <section id="about" className="about">
          <h2>About Us</h2>
          <p>We are a digital agency dedicated to creating cutting-edge solutions for businesses of all sizes.</p>
          <img
              src="https://images.unsplash.com/photo-1623479322729-28b25c16b011?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              alt="About"/>
      </section>
  );
};

const ServicesSection = () => {
    return (
        <section id="services" className="services">
            <h2>Our Services</h2>
            <div className="service-list">
                <div className="service">
                    <img src="https://images.unsplash.com/photo-1525373698358-041e3a460346?q=80&w=1528&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" width="350" height="300"/>
                <h3>Web Design</h3>
          <p>Strong visual branding is crucial for business success. Our team of graphic designers crafts unique and engaging graphics that effectively communicate your brand message. From logo designs to print materials and social media graphics – we provide custom solutions that highlight your brand and resonate with your target <audience className=""></audience></p>
        </div>
                <div className="service">
                    <img
                        src="https://images.unsplash.com/photo-1526948531399-320e7e40f0ca?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                        width="350" height="300"/>
                    <h3>Consulting</h3>
                    <p>Interested in learning more about our web design and graphic solutions? We’d love to hear from
                        you! Contact us today to discuss your next project. Our team is ready to answer your questions
                        and provide a no-obligation consultation.</p>
                </div>
                <div className="service">
                    <img
                        src="https://images.unsplash.com/photo-1538688423619-a81d3f23454b?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTl8fG1hbmFnbWVudHxlbnwwfHwwfHx8MA%3D%3D"
                        width="350" height="300"/>
                    <h3>Management</h3>
                    <p>We believe that effective management is the cornerstone of success. We take pride in our ability
                        to guide businesses and teams towards achieving their full potential. Our experienced management
                        team possesses a deep understanding of the ever-evolving business landscape</p>
                </div>
            </div>
        </section>
    );
};

const PortfolioSection = () => {
    return (
        <section id="portfolio" className="portfolio">
            <h2>Achieving your goals requires more than just advice—it demands a clear vision.</h2>
            <div className="portfolio-list">
                <div className="portfolio-item">
                    <img width="400" height="856"
                         src="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubai-custom-logos-770x1024.jpg"
                         className="attachment-large size-large wp-image-438" alt="PixelPerfect Dubai custom logos"
                         srcSet="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubai-custom-logos-770x1024.jpg 770w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubai-custom-logos-226x300.jpg 226w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubai-custom-logos-768x1021.jpg 768w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubai-custom-logos-9x12.jpg 9w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubai-custom-logos.jpg 1105w"
                         sizes="100vw"/>
                </div>
                <div className="portfolio-item">
                    <img width="400" height="856"
                         src="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubau-Website-SEOjpg-770x1024.jpg"
                         className="attachment-large size-large wp-image-437" alt="PixelPerfect Dubau Website SEOjpg"
                         srcSet="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubau-Website-SEOjpg-770x1024.jpg 770w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubau-Website-SEOjpg-226x300.jpg 226w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubau-Website-SEOjpg-768x1021.jpg 768w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubau-Website-SEOjpg-9x12.jpg 9w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Dubau-Website-SEOjpg.jpg 1105w"
                         sizes="100vw"/>
                </div>
                <div className="portfolio-item">
                    <img width="400" height="856"
                         src="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Management-Dubai-770x1024.jpg"
                         className="attachment-large size-large wp-image-436" alt="PixelPerfect Management Dubai"
                         srcSet="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Management-Dubai-770x1024.jpg 770w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Management-Dubai-226x300.jpg 226w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Management-Dubai-768x1021.jpg 768w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Management-Dubai-9x12.jpg 9w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Management-Dubai.jpg 1105w"
                         sizes="100vw"/>
                </div>
                <div className="portfolio-item">
                    <img width="400" height="122"
                         src="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Digital-Signage-Dubai-770x1024.jpg"
                         className="attachment-large size-large wp-image-435" alt=""
                         srcSet="https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Digital-Signage-Dubai-770x1024.jpg 770w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Digital-Signage-Dubai-226x300.jpg 226w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Digital-Signage-Dubai-768x1021.jpg 768w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Digital-Signage-Dubai-9x12.jpg 9w, https://pixelperfect-solutions.com/wp-content/uploads/2023/08/PixelPerfect-Digital-Signage-Dubai.jpg 1105w"
                         sizes="100vw"/>
                </div>
                </div>
        </section>
);
};

const ContactSection = () => {
    return (
        <section id="contact" className="contact">
            <h2>Contact Us</h2>
            <form>
                <label>
                    Name:
                    <input type="text" name="name" placeholder="John"/>
                </label>
                <label>
                    Email:
                    <input type="email" name="email" placeholder="xxxx@gmail.com"/>
                </label>
                <label>
                    Message:
                    <textarea name="message" placeholder="Type...."></textarea>
                </label>
                <button type="submit">Submit</button>
            </form>
        </section>
    );
};

const Footer = () => {
    return (
        <footer className="footer">
            <p>&copy; 2024 Pixel Perfect Solutions. All rights reserved.</p>
        </footer>
    );
};

export default App;
