import React from 'react';
import './App.css';

function App() {
  return (
    <div className="app">
      <Header />
      <HomePage />
      <Footer />
    </div>
  );
}

const Header = () => {
  return (
    <header className="header">
      <div className="logo">Pixel Perfect Solutions</div>
      <nav>
        <ul>
          <li><a href="#home">Home</a></li>
          <li><a href="#about">About</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portfolio">Portfolio</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
  );
};

const HomePage = () => {
  return (
    <div>
      <HeroSection />
      <AboutSection />
      <ServicesSection />
      <PortfolioSection />
      <ContactSection />
    </div>
  );
};

const HeroSection = () => {
  return (
    <section id="home" className="hero">
      <h1>Welcome to Pixel Perfect Solutions</h1>
      <p>Your partner in digital transformation</p>
      <button>Get Started</button>
    </section>
  );
};

const AboutSection = () => {
  return (
    <section id="about" className="about">
      <h2>About Us</h2>
      <p>We are a digital agency dedicated to creating cutting-edge solutions for businesses of all sizes.</p>
      <img src="https://via.placeholder.com/600x400" alt="About Us" />
    </section>
  );
};

const ServicesSection = () => {
  return (
    <section id="services" className="services">
      <h2>Our Services</h2>
      <div className="service-list">
        <div className="service">
          <h3>Service One</h3>
          <p>Details about Service One</p>
        </div>
        <div className="service">
          <h3>Service Two</h3>
          <p>Details about Service Two</p>
        </div>
        <div className="service">
          <h3>Service Three</h3>
          <p>Details about Service Three</p>
        </div>
      </div>
    </section>
  );
};

const PortfolioSection = () => {
  return (
    <section id="portfolio" className="portfolio">
      <h2>Our Portfolio</h2>
      <div className="portfolio-list">
        <div className="portfolio-item">
          <img src="https://picsum.photos/200/300.jpg" alt="Project 1" />
          <h3>Project 1</h3>
          <p>Description of Project 1</p>
        </div>
        <div className="portfolio-item">
          <img src="https://picsum.photos/200/300.jpg" alt="Project 2" />
          <h3>Project 2</h3>
          <p>Description of Project 2</p>
        </div>
        <div className="portfolio-item">
          <img src="https://picsum.photos/200/300.jpg" alt="Project 3" />
          <h3>Project 3</h3>
          <p>Description of Project 3</p>
        </div>
      </div>
    </section>
  );
};

const ContactSection = () => {
  return (
    <section id="contact" className="contact">
      <h2>Contact Us</h2>
      <form>
        <label>
          Name:
          <input type="text" name="name" />
        </label>
        <label>
          Email:
          <input type="email" name="email" />
        </label>
        <label>
          Message:
          <textarea name="message"></textarea>
        </label>
        <button type="submit">Submit</button>
      </form>
    </section>
  );
};

const Footer = () => {
  return (
    <footer className="footer">
      <p>&copy; 2024 Pixel Perfect Solutions. All rights reserved.</p>
    </footer>
  );
};

export default App;
