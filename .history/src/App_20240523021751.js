import React from 'react';
import './App.css';

function App() {
  return (
    <div className="app">
      <Header />
      <HomePage />
      <Footer />
    </div>
  );
}

const Header = () => {
  return (
    <header className="header">
      <div className="logo">Pixel Perfect Solutions</div>
      <nav>
        <ul>
          <li><a href="#home">Home</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
  );
};

const HomePage = () => {
  return (
    <div>
      <HeroSection />
      <ServicesSection />
      <ContactSection />
    </div>
  );
};

const HeroSection = () => {
  return (
    <section id="home" className="hero">
      <h1>Welcome to Pixel Perfect Solutions</h1>
      <p>Your partner in digital transformation</p>
      <button>Get Started</button>
    </section>
  );
};

const ServicesSection = () => {
  return (
    <section id="services" className="services">
      <h2>Our Services</h2>
      <div className="service-list">
        <div className="service">
          <h3>Service One</h3>
          <p>Details about Service One</p>
        </div>
        <div className="service">
          <h3>Service Two</h3>
          <p>Details about Service Two</p>
        </div>
        <div className="service">
          <h3>Service Three</h3>
          <p>Details about Service Three</p>
        </div>
      </div>
    </section>
  );
};

const ContactSection = () => {
  return (
    <section id="contact" className="contact">
      <h2>Contact Us</h2>
      <form>
        <label>
          Name:
          <input type="text" name="name" />
        </label>
        <label>
          Email:
          <input type="email" name="email" />
        </label>
        <label>
          Message:
          <textarea name="message"></textarea>
        </label>
        <button type="submit">Submit</button>
      </form>
    </section>
  );
};

const Footer = () => {
  return (
    <footer className="footer">
      <p>&copy; 2024 Pixel Perfect Solutions. All rights reserved.</p>
    </footer>
  );
};

export default App;
