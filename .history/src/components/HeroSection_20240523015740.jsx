// src/components/HeroSection.js
import React from 'react';
import styles from './HeroSection.module.css';

const HeroSection = () => {
    return (
        <section className={styles.hero}>
            <h1>Welcome to Pixel Perfect Solutions</h1>
            <p>Your partner in digital transformation</p>
            <button>Get Started</button>
        </section>
    );
};

export default HeroSection;
