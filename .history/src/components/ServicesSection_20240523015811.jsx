// src/components/ServicesSection.js
import React from 'react';
import styles from './ServicesSection.module.css';

const ServicesSection = () => {
    return (
        <section className={styles.services}>
            <h2>Our Services</h2>
            <div className={styles.serviceList}>
                <div className={styles.service}>
                    <h3>Service One</h3>
                    <p>Details about Service One</p>
                </div>
                <div className={styles.service}>
                    <h3>Service Two</h3>
                    <p>Details about Service Two</p>
                </div>
                <div className={styles.service}>
                    <h3>Service Three</h3>
                    <p>Details about Service Three</p>
                </div>
            </div>
        </section>
    );
};

export default ServicesSection;
