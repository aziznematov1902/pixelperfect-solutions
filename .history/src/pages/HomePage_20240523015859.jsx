// src/pages/HomePage.js
import React from 'react';
import HeroSection from '../components/HeroSection';
import ServicesSection from '../components/ServicesSection';
import ContactSection from '../components/ContactSection';

const HomePage = () => {
    return (
        <div>
            <HeroSection />
            <ServicesSection />
            <ContactSection />
        </div>
    );
};

export default HomePage;
