// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import HomePage from './pages/HomePage';

export default function App() {
    return (
      <div>
          {/* // <Router> */}
            <Header />
            <HomePage ></HomePage>
            {/* // <Switch> */}
                {/* // <Route path="/" exact component={HomePage} /> */}
                {/* Additional routes can be added here */}
            {/* // </Switch> */}
            <Footer />
        // </Router>
      </div>
    );
  }
