import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      
      <div data-elementor-type="header" data-elementor-id="155" class="elementor elementor-155 elementor-location-header" data-elementor-post-type="elementor_library">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-53511c6 elementor-section-full_width elementor-section-height-min-height elementor-hidden-mobile elementor-section-height-default elementor-section-items-middle elementor-sticky elementor-sticky--effects elementor-sticky--active elementor-section--handles-inside" data-id="53511c6" data-element_type="section" id="hide-header" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}" style="position: fixed; width: 937px; margin-top: 0px; margin-bottom: 0px; top: 0px;">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-d3fdb54" data-id="d3fdb54" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-47182c8 elementor-align-center elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="47182c8" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
			<link rel="stylesheet" href="https://pixelperfect-solutions.com/wp-content/plugins/elementor/assets/css/widget-icon-list.min.css">		<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-clock"></i>						</span>
										<span class="elementor-icon-list-text">Mo-Sa 10:00 - 18:00</span>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-044d4f1" data-id="044d4f1" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-99a87d0 elementor-nav-menu__align-center elementor-nav-menu--stretch elementor-nav-menu__text-align-center elementor-hidden-tablet elementor-hidden-mobile elementor-nav-menu--dropdown-tablet elementor-widget elementor-widget-nav-menu" data-id="99a87d0" data-element_type="widget" data-settings="{&quot;full_width&quot;:&quot;stretch&quot;,&quot;layout&quot;:&quot;horizontal&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;<i class=\&quot;fas fa-caret-down\&quot;><\/i>&quot;,&quot;library&quot;:&quot;fa-solid&quot;}}" data-widget_type="nav-menu.default">
				<div class="elementor-widget-container">
			<link rel="stylesheet" href="https://pixelperfect-solutions.com/wp-content/plugins/elementor-pro/assets/css/widget-nav-menu.min.css">			<nav class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-horizontal e--pointer-text e--animation-none">
				<ul id="menu-1-99a87d0" class="elementor-nav-menu" data-smartmenus-id="17164060134228125"><li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher menu-item-305"><a href="https://pixelperfect-solutions.com/de/" class="elementor-item"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/de_DE.png" width="18" height="12" alt="de_DE" title="German"></span></a></li>
<li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher current-language-menu-item menu-item-306"><a href="https://pixelperfect-solutions.com/" class="elementor-item"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/en_US.png" width="18" height="12" alt="en_US" title="English"></span></a></li>
</ul>			</nav>
						<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container" aria-hidden="true" style="--menu-height: 0; width: 937px; left: 0px;">
				<ul id="menu-2-99a87d0" class="elementor-nav-menu" data-smartmenus-id="17164060134247737"><li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher menu-item-305"><a href="https://pixelperfect-solutions.com/de/" class="elementor-item" tabindex="-1"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/de_DE.png" width="18" height="12" alt="de_DE" title="German"></span></a></li>
<li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher current-language-menu-item menu-item-306"><a href="https://pixelperfect-solutions.com/" class="elementor-item" tabindex="-1"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/en_US.png" width="18" height="12" alt="en_US" title="English"></span></a></li>
</ul>			</nav>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-7c73de7" data-id="7c73de7" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-cb5aa5d elementor-align-center elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="cb5aa5d" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<a href="mailto:grafik@pixelperfect-solutions.com?subject=Contact%20PixelPerfect">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-envelope"></i>						</span>
										<span class="elementor-icon-list-text">support@pixelperfect-solutions.com</span>
											</a>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
					</div>
		</section><section class="elementor-section elementor-top-section elementor-element elementor-element-53511c6 elementor-section-full_width elementor-section-height-min-height elementor-hidden-mobile elementor-section-height-default elementor-section-items-middle elementor-sticky elementor-sticky--effects elementor-sticky__spacer" data-id="53511c6" data-element_type="section" id="hide-header" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}" style="visibility: hidden; transition: none 0s ease 0s; animation: auto ease 0s 1 normal none running none;">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-d3fdb54" data-id="d3fdb54" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-47182c8 elementor-align-center elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="47182c8" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
			<link rel="stylesheet" href="https://pixelperfect-solutions.com/wp-content/plugins/elementor/assets/css/widget-icon-list.min.css">		<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-clock"></i>						</span>
										<span class="elementor-icon-list-text">Mo-Sa 10:00 - 18:00</span>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-044d4f1" data-id="044d4f1" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-99a87d0 elementor-nav-menu__align-center elementor-nav-menu--stretch elementor-nav-menu__text-align-center elementor-hidden-tablet elementor-hidden-mobile elementor-nav-menu--dropdown-tablet elementor-widget elementor-widget-nav-menu" data-id="99a87d0" data-element_type="widget" data-settings="{&quot;full_width&quot;:&quot;stretch&quot;,&quot;layout&quot;:&quot;horizontal&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;<i class=\&quot;fas fa-caret-down\&quot;><\/i>&quot;,&quot;library&quot;:&quot;fa-solid&quot;}}" data-widget_type="nav-menu.default">
				<div class="elementor-widget-container">
			<link rel="stylesheet" href="https://pixelperfect-solutions.com/wp-content/plugins/elementor-pro/assets/css/widget-nav-menu.min.css">			<nav class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-horizontal e--pointer-text e--animation-none">
				<ul id="menu-1-99a87d0" class="elementor-nav-menu" data-smartmenus-id="17164060134228125"><li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher menu-item-305"><a href="https://pixelperfect-solutions.com/de/" class="elementor-item"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/de_DE.png" width="18" height="12" alt="de_DE" title="German"></span></a></li>
<li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher current-language-menu-item menu-item-306"><a href="https://pixelperfect-solutions.com/" class="elementor-item"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/en_US.png" width="18" height="12" alt="en_US" title="English"></span></a></li>
</ul>			</nav>
						<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container" aria-hidden="true" style="--menu-height: 0; width: 948px; left: 0px;">
				<ul id="menu-2-99a87d0" class="elementor-nav-menu" data-smartmenus-id="17164060134247737"><li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher menu-item-305"><a href="https://pixelperfect-solutions.com/de/" class="elementor-item" tabindex="-1"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/de_DE.png" width="18" height="12" alt="de_DE" title="German"></span></a></li>
<li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher current-language-menu-item menu-item-306"><a href="https://pixelperfect-solutions.com/" class="elementor-item" tabindex="-1"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/en_US.png" width="18" height="12" alt="en_US" title="English"></span></a></li>
</ul>			</nav>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-7c73de7" data-id="7c73de7" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-cb5aa5d elementor-align-center elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="cb5aa5d" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<a href="mailto:grafik@pixelperfect-solutions.com?subject=Contact%20PixelPerfect">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-envelope"></i>						</span>
										<span class="elementor-icon-list-text">support@pixelperfect-solutions.com</span>
											</a>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
					</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-18f3942 elementor-section-full_width elementor-section-height-min-height elementor-hidden-desktop elementor-hidden-tablet elementor-section-height-default elementor-section-items-middle elementor-sticky elementor-sticky--effects elementor-sticky--active elementor-section--handles-inside" data-id="18f3942" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}" style="position: fixed; margin-top: 0px; margin-bottom: 0px; top: 0px;">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-20e00b6" data-id="20e00b6" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-12a148d elementor-align-center elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="12a148d" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<a href="mailto:suport@pixelperfect-solutions.com?subject=Contact%20PixelPerfect">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-envelope"></i>						</span>
										<span class="elementor-icon-list-text">support@pixelperfect-solutions.com</span>
											</a>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
					</div>
		</section><section class="elementor-section elementor-top-section elementor-element elementor-element-18f3942 elementor-section-full_width elementor-section-height-min-height elementor-hidden-desktop elementor-hidden-tablet elementor-section-height-default elementor-section-items-middle elementor-sticky elementor-sticky--effects elementor-sticky__spacer" data-id="18f3942" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}" style="visibility: hidden; transition: none 0s ease 0s; animation: auto ease 0s 1 normal none running none;">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-20e00b6" data-id="20e00b6" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-12a148d elementor-align-center elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="12a148d" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<a href="mailto:suport@pixelperfect-solutions.com?subject=Contact%20PixelPerfect">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-envelope"></i>						</span>
										<span class="elementor-icon-list-text">support@pixelperfect-solutions.com</span>
											</a>
									</li>
						</ul>
				</div>
				</div>
					</div>
		</div>
					</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-ac677c7 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle" data-id="ac677c7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:245,&quot;url&quot;:&quot;https:\/\/pixelperfect-solutions.com\/wp-content\/uploads\/2023\/08\/woman-use-laptop-work-cafe-scaled.jpg&quot;},{&quot;id&quot;:246,&quot;url&quot;:&quot;https:\/\/pixelperfect-solutions.com\/wp-content\/uploads\/2023\/08\/homemade-recipe-food-nutrition-appetite-scaled.jpg&quot;},{&quot;id&quot;:243,&quot;url&quot;:&quot;https:\/\/pixelperfect-solutions.com\/wp-content\/uploads\/2023\/08\/closeup-hands-using-mobile-phone-computer-laptop-black-marble-table-scaled.jpg&quot;}],&quot;background_slideshow_slide_duration&quot;:3000,&quot;background_slideshow_transition_duration&quot;:4000,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;}"><div class="elementor-background-slideshow swiper swiper-fade swiper-initialized swiper-horizontal swiper-pointer-events swiper-rtl swiper-watch-progress swiper-backface-hidden" dir="rtl"><div class="swiper-wrapper" id="swiper-wrapper-8fa56051216315f9" aria-live="off" style="transition-duration: 4000ms;"><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate" data-swiper-slide-index="0" role="group" aria-label="1 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(0px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/woman-use-laptop-work-cafe-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate" data-swiper-slide-index="1" role="group" aria-label="2 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(937px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/homemade-recipe-food-nutrition-appetite-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="2" role="group" aria-label="3 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(1874px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/closeup-hands-using-mobile-phone-computer-laptop-black-marble-table-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate-active" data-swiper-slide-index="0" role="group" aria-label="1 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(2811px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/woman-use-laptop-work-cafe-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate-next" data-swiper-slide-index="1" role="group" aria-label="2 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(3748px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/homemade-recipe-food-nutrition-appetite-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-prev" data-swiper-slide-index="2" role="group" aria-label="3 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(4685px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/closeup-hands-using-mobile-phone-computer-laptop-black-marble-table-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate swiper-slide-visible swiper-slide-active" data-swiper-slide-index="0" role="group" aria-label="1 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 1; transform: translate3d(5622px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/woman-use-laptop-work-cafe-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate swiper-slide-next" data-swiper-slide-index="1" role="group" aria-label="2 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(6559px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/homemade-recipe-food-nutrition-appetite-scaled.jpg&quot;);"></div></div><div class="elementor-background-slideshow__slide swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="2" role="group" aria-label="3 / 3" style="width: 937px; transition-duration: 4000ms; opacity: 0; transform: translate3d(7496px, 0px, 0px);"><div class="elementor-background-slideshow__slide__image" style="background-image: url(&quot;https://pixelperfect-solutions.com/wp-content/uploads/2023/08/closeup-hands-using-mobile-phone-computer-laptop-black-marble-table-scaled.jpg&quot;);"></div></div></div><span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-d2896c9" data-id="d2896c9" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-f0f5220 elementor-headline--style-rotate elementor-widget elementor-widget-animated-headline" data-id="f0f5220" data-element_type="widget" data-settings="{&quot;headline_style&quot;:&quot;rotate&quot;,&quot;animation_type&quot;:&quot;swirl&quot;,&quot;rotating_text&quot;:&quot;Webdesign - Graphic&quot;,&quot;rotate_iteration_delay&quot;:100,&quot;loop&quot;:&quot;yes&quot;}" data-widget_type="animated-headline.default">
				<div class="elementor-widget-container">
			<link rel="stylesheet" href="https://pixelperfect-solutions.com/wp-content/plugins/elementor-pro/assets/css/widget-animated-headline.min.css">		<h1 class="elementor-headline elementor-headline-animation-type-swirl elementor-headline-letters">
				<span class="elementor-headline-dynamic-wrapper elementor-headline-text-wrapper" style="width: 349.344px;">
					<span class="elementor-headline-dynamic-text elementor-headline-text-active" style="opacity: 1;"><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">
</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">	</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">	</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">	</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">	</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">W</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">e</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">b</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">d</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">e</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">s</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">i</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">g</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">n</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">&nbsp;</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">-</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">&nbsp;</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">G</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">r</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">a</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">p</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">h</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">i</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">c</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">	</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">	</span><span class="elementor-headline-dynamic-letter elementor-headline-animation-in">	</span></span>
						</span>
				</h1>
				</div>
				</div>
					</div>
		</div>
					</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-7c4801c elementor-section-full_width elementor-section-height-min-height elementor-hidden-mobile elementor-hidden-tablet elementor-section-height-default elementor-section-items-middle" data-id="7c4801c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-45ee5472" data-id="45ee5472" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-4057d410 elementor-widget elementor-widget-image" data-id="4057d410" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
			<style>/*! elementor - v3.21.0 - 22-05-2024 */
.elementor-widget-image{text-align:center}.elementor-widget-image a{display:inline-block}.elementor-widget-image a img[src$=".svg"]{width:48px}.elementor-widget-image img{vertical-align:middle;display:inline-block}</style>											<a href="https://pixelperfect-solutions.com">
							<img width="644" height="119" src="https://pixelperfect-solutions.com/wp-content/uploads/2023/06/PixelPerfekt-1024x189.png" class="attachment-large size-large wp-image-7" alt="" srcset="https://pixelperfect-solutions.com/wp-content/uploads/2023/06/PixelPerfekt-1024x189.png 1024w, https://pixelperfect-solutions.com/wp-content/uploads/2023/06/PixelPerfekt-300x55.png 300w, https://pixelperfect-solutions.com/wp-content/uploads/2023/06/PixelPerfekt-768x142.png 768w, https://pixelperfect-solutions.com/wp-content/uploads/2023/06/PixelPerfekt-1536x283.png 1536w, https://pixelperfect-solutions.com/wp-content/uploads/2023/06/PixelPerfekt-2048x378.png 2048w" sizes="100vw">								</a>
													</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-3c9c325" data-id="3c9c325" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-421915c elementor-nav-menu--stretch elementor-nav-menu__text-align-center elementor-hidden-desktop elementor-hidden-tablet elementor-widget__width-initial elementor-nav-menu--toggle elementor-nav-menu--burger elementor-widget elementor-widget-nav-menu" data-id="421915c" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;dropdown&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;<i class=\&quot;fas fa-caret-down\&quot;><\/i>&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;toggle&quot;:&quot;burger&quot;}" data-widget_type="nav-menu.default">
				<div class="elementor-widget-container">
					<div class="elementor-menu-toggle" role="button" tabindex="0" aria-label="Menu Toggle" aria-expanded="false">
			<i aria-hidden="true" role="presentation" class="elementor-menu-toggle__icon--open eicon-menu-bar"></i><i aria-hidden="true" role="presentation" class="elementor-menu-toggle__icon--close eicon-close"></i>			<span class="elementor-screen-only">Menu</span>
		</div>
					<nav class="elementor-nav-menu--dropdown elementor-nav-menu__container" aria-hidden="true" style="top: 0px; --menu-height: 0; width: 937px; left: 0px;">
				<ul id="menu-2-421915c" class="elementor-nav-menu" data-smartmenus-id="17164060134294634"><li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher menu-item-305"><a href="https://pixelperfect-solutions.com/de/" class="elementor-item" tabindex="-1"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/de_DE.png" width="18" height="12" alt="de_DE" title="German"></span></a></li>
<li class="trp-language-switcher-container menu-item menu-item-type-post_type menu-item-object-language_switcher current-language-menu-item menu-item-306"><a href="https://pixelperfect-solutions.com/" class="elementor-item" tabindex="-1"><span data-no-translation=""><img class="trp-flag-image" src="https://pixelperfect-solutions.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/en_US.png" width="18" height="12" alt="en_US" title="English"></span></a></li>
</ul>			</nav>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-e8d6033" data-id="e8d6033" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-a8b1473 elementor-widget elementor-widget-heading" data-id="a8b1473" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<style>/*! elementor - v3.21.0 - 22-05-2024 */
{/* .elementor-heading-title{padding:0;margin:0;line-height:1}.elementor-widget-heading .elementor-heading-title[class*=elementor-size-]>a{color:inherit;font-size:inherit;line-height:inherit}.elementor-widget-heading .elementor-heading-title.elementor-size-small{font-size:15px}.elementor-widget-heading .elementor-heading-title.elementor-size-medium{font-size:19px}.elementor-widget-heading .elementor-heading-title.elementor-size-large{font-size:29px}.elementor-widget-heading .elementor-heading-title.elementor-size-xl{font-size:39px}.elementor-widget-heading .elementor-heading-title.elementor-size-xxl{font-size:59px}</style><span class="elementor-heading-title elementor-size-medium"><a href="#graphic">Graphic</a></span>		</div> */}
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-f73daee" data-id="f73daee" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-b15862b elementor-widget elementor-widget-heading" data-id="b15862b" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<span class="elementor-heading-title elementor-size-medium"><a href="#digitalsignage">Digital signage</a></span>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-top-column elementor-element elementor-element-25cc316" data-id="25cc316" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
						<div class="elementor-element elementor-element-927ca15 elementor-widget elementor-widget-heading" data-id="927ca15" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_mouse&quot;:&quot;yes&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<span class="elementor-heading-title elementor-size-medium"><a href="#webdesign">Webdesign</a></span>		</div>
				</div>
					</div>
		</div>
					</div>
		</section>
				</div>
    </div>
  );
}

export default App;
